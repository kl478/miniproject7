use anyhow::{Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, Condition, Filter, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;


#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "test_collection";
    create_collection(&client, collection_name).await?;
    ingest_vectors(&client, collection_name).await?;
    query_vectors(&client, collection_name).await?;
    query_vectors_with_filter(&client, collection_name).await?;

    Ok(())
}

async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}


async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Delete collection if it already exists
    let _ = client.delete_collection(collection_name).await;

    // Create collection with desired configuration
    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}


async fn ingest_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Create 3 different vectors representing technological products
    let points = vec![
        PointStruct::new(
            1,
            vec![0.8, 0.9, 0.7, 0.95],
            json!({"product": "Gaming Laptop", "category": "Computers"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.6, 0.85, 0.9, 0.8],
            json!({"product": "Smartphone Pro", "category": "Mobile Phones"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![0.75, 0.65, 0.85, 0.9],
            json!({"product": "E-Reader", "category": "Gadgets"}).try_into().unwrap(),
        ),
        PointStruct::new(
            4,
            vec![0.55, 0.8, 0.65, 0.9],
            json!({"product": "Smart Watch", "category": "Wearables"}).try_into().unwrap(),
        ),
        PointStruct::new(
            5,
            vec![0.65, 0.9, 0.85, 0.95],
            json!({"product": "High-End PC", "category": "Computers"}).try_into().unwrap(),
        ),
        PointStruct::new(
            6,
            vec![0.4, 0.7, 0.9, 0.6],
            json!({"product": "Noise Cancelling Headphones", "category": "Audio"}).try_into().unwrap(),
        ),
        PointStruct::new(
            7,
            vec![0.3, 0.9, 0.6, 0.7],
            json!({"product": "Portable Speaker", "category": "Audio"}).try_into().unwrap(),
        ),
        PointStruct::new(
            8,
            vec![0.7, 0.85, 0.75, 0.95],
            json!({"product": "4K Monitor", "category": "Computers"}).try_into().unwrap(),
        ),
        PointStruct::new(
            9,
            vec![0.8, 0.7, 0.9, 0.85],
            json!({"product": "Tablet", "category": "Mobile Devices"}).try_into().unwrap(),
        ),
        PointStruct::new(
            10,
            vec![0.5, 0.65, 0.8, 0.9],
            json!({"product": "Fitness Tracker", "category": "Wearables"}).try_into().unwrap(),
        ),

    ];

    // Upsert points into the collection
    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();

    Ok(())
}


async fn query_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let query_vector = vec![0.5, 0.9, 0.9, 0.9];

    // Run a query on the data with the updated query vector
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: query_vector,
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("Search Results for Technological Products:");

    // Iterate over each found point
    for (index, point) in search_result.result.iter().enumerate() {
        // Format the payload to display product information nicely
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        // Print each point's product information and match score
        println!("Product {}: ", index + 1);
        println!(" - Details: {}", formatted_payload);
        println!(" - Match Score: {}", point.score); // Reflects how closely the product matches the query
        println!(); // Add an empty line for readability
    }

    Ok(())
}


async fn query_vectors_with_filter(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.5, 0.9, 0.9, 0.9], // Attributes vector
            filter: Some(Filter::all([Condition::matches(
                "category",
                "Computers".to_string(),
            )])),
            limit: 2, // for broader aggregation
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;
    println!("-------------------------------------------------------------\n");
    println!("Filtered Search Results: Top Matches in 'Computers' Category:");
    
    let mut total_score = 0.0f32; // Ensure total_score is f32
    let results_count = search_result.result.len() as f32; // Convert results_count to f32

    for (index, point) in search_result.result.iter().enumerate() {
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        println!("Filtered Product {}: ", index + 1);
        println!(" - Details: {}", formatted_payload);
        println!(" - Match Score: {}", point.score);
        println!();

        total_score += point.score; 
    }

    if results_count > 0.0 {
        let average_score = total_score / results_count; // Both operands are now f32
        println!("Average Match Score for 'Computers': {}", average_score);
    } else {
        println!("No products found in 'Computers' category.");
    }

    Ok(())
}
