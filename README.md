# Mini Project 7: Data Processing with Vector Database

## Goals
- Ingest data into a Vector database (Qdrant)
- Perform advanced queries and aggregations
- Visualize the query outputs

## Project Description
This project focuses on leveraging Qdrant, a vector search engine, for efficient data processing and analysis. It demonstrates ingesting data, performing complex queries with filters, aggregating results, and visualizing these operations. The project showcases Rust's ability to interact with vector databases, offering insights into handling and analyzing high-dimensional data.

## Function Description
The core functionality involves:
1. Establishing a connection to the Qdrant vector database.
2. Creating a data collection for technological product vectors.
3. Ingesting data into the collection, including product details and categories.
4. Executing queries to search for products based on vector similarity and specific filters (e.g., product categories).
5. Aggregating query results to compute statistics such as average scores.
6. Visualizing the search and aggregation results in the console.

## Key Steps
1. Ensure Rust and Docker are installed on your system.
2. Create a new Lambda project and navigate into it:
    ```bash
    cargo lambda new <Your Project Name> && cd <Your Project Name>
    ```
3. Add necessary dependencies to `Cargo.toml`:
   ```toml
    [dependencies]
    qdrant-client = "1.8.0"
    tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
    serde_json = "1.0.114"
    tonic = "0.11.0"
    anyhow = "1.0.81"
   ```
4. Set up and run the Qdrant vector database using Docker:
   ```bash
   docker pull qdrant/qdrant
   docker run -p 6333:6333 -p 6334:6334 \
       -e QDRANT__SERVICE__GRPC_PORT="6334" \
       qdrant/qdrant
   ```
5. Implement the data ingestion, querying, and aggregation functionality in `main.rs`.
6. Execute the project to ingest data, perform queries, and visualize results:
   ```bash
   cargo run
   ```


## Data Ingestion
The data ingestion process involves several steps, including initializing the Qdrant client, creating a data collection, and ingesting vectors into the collection. This project ingests vectors representing technological products, each with details such as product name and category. 

### Process
1. **Initialize Qdrant Client**: Setup the Qdrant client to connect to the vector database server.
2. **Create Collection**: A new collection named "test_collection" is created with vector parameters configured for optimal storage and retrieval.
3. **Ingest Vectors**: Vectors representing various technological products are upserted into the collection. Each vector is associated with a payload containing product details.

### Example Vector
```rust
PointStruct::new(
    1,
    vec![0.8, 0.9, 0.7, 0.95],
    json!({"product": "Gaming Laptop", "category": "Computers"}).try_into().unwrap(),
),
```

## Query Functionality & Aggregation
Upon populating our database with diverse product data, we delve into the querying capabilities of Qdrant. Our focus here is two-fold: 

1. To retrieve products based on their vector similarity to a query vector, highlighting the essence of vector databases in identifying similar items. 

2. We introduce filters to these queries, such as product categories, to refine our search results further and demonstrate the database's flexibility in handling complex search criteria.

3. We calculate average match scores for products within specific categories. This step not only provides valuable insights into the dataset but also illustrates how vector databases can be utilized for aggregating and analyzing data beyond simple retrieval tasks.

## Visualization
We present the outcomes of our queries and aggregations through concise console outputs, enabling an immediate understanding of the data's characteristics and the effectiveness of our queries. This visualization step is pivotal in closing the loop of our data processing journey, offering tangible results that can be leveraged for further analysis or reporting.


## Deliverables

- Database Connection
![DB](screenshots/DB.png)

- Docker Container
![Docker](screenshots/docker.png)

- DB Logs After Querying
![enable](screenshots/logs.png)

- Visualize Output of the Query Results
![Query](screenshots/query.png)
